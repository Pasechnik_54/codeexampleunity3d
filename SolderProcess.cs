﻿
using UnityEngine;

namespace Soldering
{
    // компонент, отвечающий за правила процесса пайки (в каком случае припаяется, в каком нет)
    [AddComponentMenu("User_Soldering/SolderProcess")]
    public class SolderProcess : AbstractPartialSolderingProcess, ISolderProcess
    {               
        private IHeating _heating = null;   
        private ISolderState _solderState = null;
        private IOverheat _overheat = null;

        private SolderTimer _timerTargetTemperature = null,
                            _timerOverheat = null;
        

        private void Start()
        {
            GetRequiredComponents();

            _timerTargetTemperature = new SolderTimer();
            _timerOverheat = new SolderTimer();
        }

        protected override void GetRequiredComponents()
        {
            _heating = GetComponent<Heating>();
            _solderState = GetComponent<SolderState>();
            _overheat = GetComponent<IOverheat>();
        }



        [SerializeField] private float _timeForCompletedSoldering = 4.0F;    //  задать в Inspector        
        [SerializeField] private float _timeForOverheatReaction = 2.0F;

        private float _timeAtTargetTemperature = 0; // время в зоне нужной температуры
        private float _timeAtOverheatTemperature = 0;  // время в зоне перегрева

        // вызывается в FixedUpdate() !! при необходимости оптимизировать
        public void CalculationSoldering()  
        {
            if (_solderState.CurrentState == SolderState.State.Process)
                SolderingProcess();
        }
              
        
        private void SolderingProcess ()
        {
            CountingSolderingTime();
            ReactionToSolderingTime();
        }


        private void CountingSolderingTime()
        {
            if (SolderingTemperatureOk())
            {
                _timerTargetTemperature.CountTime(ref _timeAtTargetTemperature);
            }
            else if (SolderingTemperatureOk() != true)
            {
                _timerTargetTemperature.ClearCounterTime(ref _timeAtTargetTemperature);

                if (SolderingTemperatureOverheat())
                    _timerOverheat.CountTime(ref _timeAtOverheatTemperature);
                else if (SolderingTemperatureOverheat() != true)
                    _timerOverheat.ClearCounterTime(ref _timeAtOverheatTemperature);
            }

        }       

        // макс и мин. значения температуры, при которой компонент припаивается
        private const float MIN_SOLDERING_OK_TEMPERATURE = 290;
        public float MinSolderingOkTemperature { get { return MIN_SOLDERING_OK_TEMPERATURE; } }

        private const int MAX_SOLDERING_OK_TEMPERATURE = 340;
        public float MaxSolderingOkTemperature { get { return MAX_SOLDERING_OK_TEMPERATURE; } }

        private bool SolderingTemperatureOk()   // возвращает true, если температура в диапазоне, нужном для пайки
        {
            if (_heating.CurrentTemperature > MIN_SOLDERING_OK_TEMPERATURE &&
                _heating.CurrentTemperature < MAX_SOLDERING_OK_TEMPERATURE)
                return true;
            else
                return false;
        }

        private bool SolderingTemperatureOverheat()
        {
            if (_heating.CurrentTemperature > MAX_SOLDERING_OK_TEMPERATURE)
                return true;
            else
                return false;
        }


        private void ReactionToSolderingTime()
        {
            // если выдержали положенное время в норм температуре в процессе пайки, компонент припаивается            
            if (_timeAtTargetTemperature > _timeForCompletedSoldering)
                _solderState.CurrentState = SolderState.State.Ok;

            // если передержали температуру в красной зоне, запускаем реакцию на перегрев (разрушение)
            if (_timeAtOverheatTemperature > _timeForOverheatReaction)
                _overheat.ReactionOfOverheating();
        }

    }
}
