﻿
using System;

using UnityEngine;

using ObjectNet = System.Object;

namespace Soldering
{   
    // компонент отвечает за вывод индикации температуры
    [RequireComponent(typeof(ITemperatureIndicator))]
    public class TemperatureScaleController : MonitorsSolderingMode
    {
        [SerializeField] private GameObject _scalePanel = null;

        private ITemperatureIndicator _indicator = null;



        private void Awake()
        {          
            SubscribeForEventsSwitchingSolderMode();
        }

        private void Start()
        {
            GetRequiredComponents();
        }

        protected override void GetRequiredComponents()
        {
            _indicator = GetComponent<ITemperatureIndicator>();
        }



        // Основной цикл индикации температуры
        private void Update()
        {
            if(_solderingModeActive)                 // в режиме пайки, показываем температуру
                _indicator.IndicateTemperature();
        }
        


        // переопределение реакции на переключение ражимов пайки платы
        protected override void OnSolderingMode(ObjectNet sender, EventArgs args)
        {
            _scalePanel.SetActive(true); // включаем отображение индикатора температуры
            _solderingModeActive = true;
        }

        protected override void OffSolderingMode(ObjectNet sender, EventArgs args)
        {
            _scalePanel.SetActive(false); // выключаем отображение индикатора температуры
            _solderingModeActive = false;
        }

    }
}
