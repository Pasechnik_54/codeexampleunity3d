﻿
using UnityEngine;

namespace Soldering
{   
    // компонент для объединения процессов пайки 
    [RequireComponent(typeof (ISolderState), typeof (IHeating), typeof(ISolderProcess) )]
    [AddComponentMenu("User_Soldering/SolderController")]
    public class SolderController : MonitorsSolderingMode
    {
        private ITemperatureIndicator _indicatorTemperature = null;
        public ITemperatureIndicator IndicatorTemperature 
        { set { if (value is ITemperatureIndicator) _indicatorTemperature = value; } }

        private ISolderState _solderState = null;

        private IHeating _heatingProcess = null;

        private ISolderProcess _solderProcess = null;    
        
        

        private void Start()
        {
            GetRequiredComponents();
            SubscribeForEventsSwitchingSolderMode();

            _indicatorTemperature.TransmitCurrentParametersForIndication(_heatingProcess,
                                                                          _solderProcess);
        } 
        
        protected override void GetRequiredComponents()
        {
            _solderState = GetComponent<ISolderState>();
            _heatingProcess = GetComponent<IHeating>();
            _solderProcess = GetComponent<ISolderProcess>();
        }
        

        // по касанию компонента в процесссе пайки, указываем что именно температуру этого компонента надо показывать
        private void OnMouseDown()
        {
            if(_solderState.CurrentState == SolderState.State.Process)
            _indicatorTemperature.TransmitCurrentParametersForIndication(_heatingProcess,
                                                                           _solderProcess);
        }



        // Основной цикл пайки
        private void FixedUpdate()
        {
            _heatingProcess.CalculationTemperature();
            _solderProcess.CalculationSoldering();
        }        
        
    }
}
