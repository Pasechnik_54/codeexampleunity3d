﻿
using System;

using Checking;

namespace Soldering
{
    // компонент, отвечающий за  вкл-выкл режима пайки на плате
    public class SolderSwitch : AbstractBoardSwitch, ISolderSwitch
    {
        private bool _solderingModeInBoardOn;        // статус режима пайки платы
        public bool SolderingModeInBoardOn { get; }



        public void OnMouseDown()   
        {
            ToggleProcessSoldering(); // вкл-выкл процесс пайки всех паечных компонентов на плате
        }
        

        public event EventHandler OnSolderingBoardMode;    // события вкл-выкл пайки платы
        public event EventHandler OffSolderingBoardMode;

        private void ToggleProcessSoldering()   
        {
            _solderingModeInBoardOn = !_solderingModeInBoardOn; // переключаем режим пайки

           if(HaveEventsSwitchingSubscribes())   
            {  
                if (_solderingModeInBoardOn)                
                    OnSolderingBoardMode(this, new EventArgs());
                else                
                    OffSolderingBoardMode(this, new EventArgs());
            }
            
        }

        private bool HaveEventsSwitchingSubscribes()
        {
            if (Check.CheckingToNullOk(OnSolderingBoardMode, this) ||
              Check.CheckingToNullOk(OffSolderingBoardMode, this))
                return true;
            else
                return false;            
        }
    }
}
